FROM node:alpine
RUN apk add -U git
WORKDIR /opt/app
COPY package.json /opt/app/
RUN npm install --silent
COPY index.js /opt/app/
VOLUME /etc/aras
ENTRYPOINT ["npm", "start"]
