var fs = require('fs')
var Hose = require('flowr-hose')
var path = require('path')
var request = require('request')

const ARAS_DIR = process.env.ARAS_DIR || '/etc/aras/'

var hose = new Hose({
  name: 'aras-config',
  port: process.env.PORT || 3000,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

console.log("aras-config is running...")

hose.create('rasfm-scheduler:aras', {
  scheduleId: process.env.SCHEDULE_ID
}).then(function (arasFiles) {
  try {
    fs.writeFileSync(path.join(ARAS_DIR, 'aras.block'), arasFiles.block)
    fs.writeFileSync(path.join(ARAS_DIR, 'aras.schedule'), arasFiles.schedule)
  } catch (e) {
    console.error(e)
  }
})

hose.process('update#' + process.env.SCHEDULE_ID, 1, function(job, done){
  try {
    fs.writeFileSync(path.join(ARAS_DIR, 'aras.block'), job.data.block)
    fs.writeFileSync(path.join(ARAS_DIR, 'aras.schedule'), job.data.schedule)
    done()
  } catch (e) {
    done(e)
  }
})
